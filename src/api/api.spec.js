const app = require('./api')
const supertest = require('supertest')

describe('API', () => {
  let request
  beforeEach(() => {
    request = supertest(app)
  })

  test('should get entire sashelp data', done => {
    request
      .get('/sashelp')
      .expect(200, (err, res) => {
        expect(res.body.data).toHaveLength(19)
        done()
      })
  })
})